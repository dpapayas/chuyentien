package com.kurawal.chuyentien.core;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

import com.valdesekamdem.library.mdtoast.MDToast;

/**
 * Created by dpapayas on 7/22/17.
 */

public class Config {

    public static void showError(Context context, String message) {
        MDToast mdToast = MDToast.makeText(context, message, Toast.LENGTH_LONG, MDToast.TYPE_ERROR);
        mdToast.setGravity(Gravity.BOTTOM, 0, 80);
        mdToast.show();
    }

    public static void showSuccess(Context context, String message) {
        MDToast mdToast = MDToast.makeText(context, message, Toast.LENGTH_LONG, MDToast.TYPE_SUCCESS);
        mdToast.setGravity(Gravity.BOTTOM, 0, 80);
        mdToast.show();
    }
}
