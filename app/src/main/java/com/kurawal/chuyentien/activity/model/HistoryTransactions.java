package com.kurawal.chuyentien.activity.model;

/**
 * Created by dpapayas on 7/18/17.
 */

public class HistoryTransactions {

    int id;
    String title;
    String incexp;
    String transactions;
    String date;

    public HistoryTransactions(int id, String title, String incexp, String transactions, String date) {
        this.id = id;
        this.title = title;
        this.incexp = incexp;
        this.transactions = transactions;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIncexp() {
        return incexp;
    }

    public void setIncexp(String incexp) {
        this.incexp = incexp;
    }

    public String getTransactions() {
        return transactions;
    }

    public void setTransactions(String transactions) {
        this.transactions = transactions;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
