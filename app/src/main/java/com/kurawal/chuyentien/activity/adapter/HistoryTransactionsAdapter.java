package com.kurawal.chuyentien.activity.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kurawal.chuyentien.R;
import com.kurawal.chuyentien.activity.model.HistoryTransactions;

import java.util.List;

/**
 * Created by dpapayas on 7/18/17.
 */

public class HistoryTransactionsAdapter extends RecyclerView.Adapter<HistoryTransactionsAdapter.MyViewHolder> {

    private List<HistoryTransactions> historyTransactionsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvDate, tvTransactions;
        public ImageView ivStatusTransactions;

        public MyViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvDate = (TextView) view.findViewById(R.id.tvDate);
            tvTransactions = (TextView) view.findViewById(R.id.tvTransactions);
            ivStatusTransactions = (ImageView) view.findViewById(R.id.ivStatusTransactions);
        }
    }

    public HistoryTransactionsAdapter(List<HistoryTransactions> moviesList) {
        this.historyTransactionsList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_transactions, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        HistoryTransactions historyTransactions = historyTransactionsList.get(position);
        holder.tvName.setText(historyTransactions.getTitle());
        holder.tvTransactions.setText(historyTransactions.getIncexp() + " " + historyTransactions.getTransactions());
        holder.tvDate.setText(historyTransactions.getDate());
        
        if (historyTransactions.getIncexp().equals("+")) {
            holder.ivStatusTransactions.setImageResource(R.drawable.circle_th_green);
        } else {
            holder.ivStatusTransactions.setImageResource(R.drawable.circle_th_red);
        }
    }

    @Override
    public int getItemCount() {
        return historyTransactionsList.size();
    }
}