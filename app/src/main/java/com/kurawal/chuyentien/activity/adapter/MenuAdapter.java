package com.kurawal.chuyentien.activity.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kurawal.chuyentien.R;

import java.util.ArrayList;

/**
 * Created by PSD on 13-04-17.
 */

public class MenuAdapter extends BaseAdapter {
    private ArrayList<String> mOptions = new ArrayList<>();
    Activity activity;
    private LayoutInflater inflater;

    public MenuAdapter(Activity activity, ArrayList<String> options) {
        mOptions = options;
        this.activity = activity;
        this.inflater = LayoutInflater.from(activity);
    }

    @Override
    public int getCount() {
        return mOptions.size();
    }

    @Override
    public Object getItem(int position) {
        return mOptions.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final String option = mOptions.get(position);

        convertView = inflater.inflate(R.layout.item_menu_adapter, null);

        TextView txt = (TextView) convertView.findViewById(R.id.tvName);

        txt.setText(option);

        return convertView;
    }
}
