package com.kurawal.chuyentien.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.kurawal.chuyentien.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dpapayas on 7/23/17.
 */

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.etMobileNumber)
    EditText etMobileNumber;
    @BindView(R.id.btnActionRegister)
    Button btnActionRegister;
    @BindView(R.id.btnActionLogin)
    Button btnActionLogin;
    @BindView(R.id.layFooterResgister)
    LinearLayout layFooterResgister;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_register);
        ButterKnife.bind(this);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }

    @OnClick({R.id.btnActionRegister, R.id.btnActionLogin})
    public void onViewClicked(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btnActionRegister:

                intent = new Intent(RegisterActivity.this, SetPINActivity.class);
                startActivity(intent);

                break;
            case R.id.btnActionLogin:

                intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);

                break;
        }
    }
}
