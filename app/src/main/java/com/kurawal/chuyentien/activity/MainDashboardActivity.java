package com.kurawal.chuyentien.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.kurawal.chuyentien.R;
import com.kurawal.chuyentien.activity.adapter.HistoryTransactionsAdapter;
import com.kurawal.chuyentien.activity.adapter.MenuAdapter;
import com.kurawal.chuyentien.activity.model.HistoryTransactions;
import com.kurawal.chuyentien.util.AnimatorUtils;
import com.kurawal.chuyentien.util.CenteredToolbar;
import com.kurawal.chuyentien.util.DividerItemDecoration;
import com.kurawal.chuyentien.util.customfont.TextView;
import com.ogaclejapan.arclayout.ArcLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nl.psdcompany.duonavigationdrawer.views.DuoDrawerLayout;
import nl.psdcompany.duonavigationdrawer.views.DuoMenuView;
import nl.psdcompany.duonavigationdrawer.widgets.DuoDrawerToggle;

/**
 * Created by dpapayas on 7/14/17.
 */

public class MainDashboardActivity extends AppCompatActivity implements DuoMenuView.OnMenuClickListener {

    @BindView(R.id.toolbar)
    CenteredToolbar toolbar;
    @BindView(R.id.btnCardPlus)
    RelativeLayout btnCardPlus;
    @BindView(R.id.drawer)
    DuoDrawerLayout drawer;
    @BindView(R.id.tvWallet)
    TextView tvWallet;
    @BindView(R.id.revList)
    RecyclerView revList;
    @BindView(R.id.arc_layout)
    ArcLayout arcLayout;
    @BindView(R.id.menu_layout)
    FrameLayout menuLayout;
    @BindView(R.id.fab)
    Button fab;
    @BindView(R.id.btnActionGetPaid)
    RelativeLayout btnActionGetPaid;
    @BindView(R.id.btnActionSend)
    RelativeLayout btnActionSend;
    @BindView(R.id.btnActionCashout)
    RelativeLayout btnActionCashout;

    private MenuAdapter mMenuAdapter;
    private ViewHolder mViewHolder;

    private ArrayList<String> mTitles = new ArrayList<>();

    private List<HistoryTransactions> historyTransactionsList = new ArrayList<>();
    private HistoryTransactionsAdapter historyTransactionsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_menus);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mTitles = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.menuOptions)));

        mViewHolder = new ViewHolder();

        handleToolbar();

        handleDrawer();

        handleMenu();

        HistoryTransactions();

    }

    private void HistoryTransactions(){
        historyTransactionsAdapter = new HistoryTransactionsAdapter(historyTransactionsList);

        revList.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        revList.setLayoutManager(mLayoutManager);
        revList.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        revList.setItemAnimator(new DefaultItemAnimator());
        revList.setAdapter(historyTransactionsAdapter);

        prepareMovieData();
    }

    private void prepareMovieData() {
        HistoryTransactions movie = new HistoryTransactions(1, "Denny", "+", "Received", "500.000 vnd");
        historyTransactionsList.add(movie);

        movie = new HistoryTransactions(2, "Teguh", "+", "Received", "400.000 vnd");
        historyTransactionsList.add(movie);

        movie = new HistoryTransactions(3, "Teguh", "-", "Sent", "400.000 vnd");
        historyTransactionsList.add(movie);

        historyTransactionsAdapter.notifyDataSetChanged();
    }

    private void handleMenu() {
        mMenuAdapter = new MenuAdapter(this, mTitles);
        mViewHolder.mDuoMenuView.setOnMenuClickListener(this);
        mViewHolder.mDuoMenuView.setAdapter(mMenuAdapter);
    }

    private void handleToolbar() {
        setSupportActionBar(mViewHolder.mToolbar);
    }

    private void handleDrawer() {
        DuoDrawerToggle duoDrawerToggle = new DuoDrawerToggle(this,
                mViewHolder.mDuoDrawerLayout,
                mViewHolder.mToolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        mViewHolder.mDuoDrawerLayout.setDrawerListener(duoDrawerToggle);
        duoDrawerToggle.syncState();
    }

    @Override
    public void onFooterClicked() {
    }

    @Override
    public void onHeaderClicked() {

    }


    @Override
    public void onOptionClicked(int position, Object objectClicked) {

        Intent intent;

        switch (position) {


            case 0:
                break;

            case 1:

                break;


            default:

                break;
        }

        mViewHolder.mDuoDrawerLayout.closeDrawer();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (item.getItemId()) {
            case R.id.camera:


                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.btnActionGetPaid, R.id.btnActionSend, R.id.btnActionCashout, R.id.fab})
    public void onViewClicked(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btnActionGetPaid:
                intent = new Intent(MainDashboardActivity.this, GetPadiActivity.class);
                startActivity(intent);
                break;
            case R.id.btnActionSend:
                intent = new Intent(MainDashboardActivity.this, TransferActivity.class);
                startActivity(intent);
                break;
            case R.id.btnActionCashout:
                intent = new Intent(MainDashboardActivity.this, CashoutActivity.class);
                startActivity(intent);
                break;
            case R.id.fab:
                onFabClick(view);
                break;
        }
    }

    private class ViewHolder {
        private DuoDrawerLayout mDuoDrawerLayout;
        private DuoMenuView mDuoMenuView;
        private Toolbar mToolbar;

        ViewHolder() {
            mDuoDrawerLayout = (DuoDrawerLayout) findViewById(R.id.drawer);
            mDuoMenuView = (DuoMenuView) mDuoDrawerLayout.getMenuView();
            mToolbar = (Toolbar) findViewById(R.id.toolbar);
        }
    }

    private void onFabClick(View v) {
        if (v.isSelected()) {
            hideMenu();
        } else {
            showMenu();
        }
        v.setSelected(!v.isSelected());
    }

    @SuppressWarnings("NewApi")
    private void showMenu() {
        menuLayout.setVisibility(View.VISIBLE);

        List<Animator> animList = new ArrayList<>();

        for (int i = 0, len = arcLayout.getChildCount(); i < len; i++) {
            animList.add(createShowItemAnimator(arcLayout.getChildAt(i)));
        }

        AnimatorSet animSet = new AnimatorSet();
        animSet.setDuration(400);
        animSet.setInterpolator(new OvershootInterpolator());
        animSet.playTogether(animList);
        animSet.start();
    }

    @SuppressWarnings("NewApi")
    private void hideMenu() {

        List<Animator> animList = new ArrayList<>();

        for (int i = arcLayout.getChildCount() - 1; i >= 0; i--) {
            animList.add(createHideItemAnimator(arcLayout.getChildAt(i)));
        }

        AnimatorSet animSet = new AnimatorSet();
        animSet.setDuration(400);
        animSet.setInterpolator(new AnticipateInterpolator());
        animSet.playTogether(animList);
        animSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                menuLayout.setVisibility(View.INVISIBLE);
            }
        });
        animSet.start();

    }

    private Animator createShowItemAnimator(View item) {

        float dx = fab.getX() - item.getX();
        float dy = fab.getY() - item.getY();

        item.setRotation(0f);
        item.setTranslationX(dx);
        item.setTranslationY(dy);

        Animator anim = ObjectAnimator.ofPropertyValuesHolder(
                item,
                AnimatorUtils.rotation(0f, 720f),
                AnimatorUtils.translationX(dx, 0f),
                AnimatorUtils.translationY(dy, 0f)
        );

        return anim;
    }

    private Animator createHideItemAnimator(final View item) {
        float dx = fab.getX() - item.getX();
        float dy = fab.getY() - item.getY();

        Animator anim = ObjectAnimator.ofPropertyValuesHolder(
                item,
                AnimatorUtils.rotation(720f, 0f),
                AnimatorUtils.translationX(0f, dx),
                AnimatorUtils.translationY(0f, dy)
        );

        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                item.setTranslationX(0f);
                item.setTranslationY(0f);
            }
        });

        return anim;
    }
}