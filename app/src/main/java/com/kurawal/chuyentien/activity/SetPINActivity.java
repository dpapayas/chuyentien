package com.kurawal.chuyentien.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.kurawal.chuyentien.R;
import com.kurawal.chuyentien.util.customfont.Button;
import com.kurawal.chuyentien.util.customfont.TextView;
import com.valdesekamdem.library.mdtoast.MDToast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dpapayas on 7/24/17.
 */

public class SetPINActivity extends AppCompatActivity {

    @BindView(R.id.tvSetPIN)
    TextView tvSetPIN;
    @BindView(R.id.etCountPIN)
    EditText etCountPIN;
    @BindView(R.id.pin1)
    android.widget.TextView pin1;
    @BindView(R.id.pin2)
    android.widget.TextView pin2;
    @BindView(R.id.pin3)
    android.widget.TextView pin3;
    @BindView(R.id.pin4)
    android.widget.TextView pin4;
    @BindView(R.id.layWelcomeText)
    LinearLayout layWelcomeText;
    @BindView(R.id.b1)
    Button b1;
    @BindView(R.id.b2)
    Button b2;
    @BindView(R.id.b3)
    Button b3;
    @BindView(R.id.b4)
    Button b4;
    @BindView(R.id.b5)
    Button b5;
    @BindView(R.id.b6)
    Button b6;
    @BindView(R.id.b7)
    Button b7;
    @BindView(R.id.b8)
    Button b8;
    @BindView(R.id.b9)
    Button b9;
    @BindView(R.id.b)
    android.widget.Button b;
    @BindView(R.id.b0)
    Button b0;
    @BindView(R.id.bDelete)
    RelativeLayout bDelete;
    @BindView(R.id.btnActionNext)
    Button btnActionNext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_verify);
        ButterKnife.bind(this);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }


        etCountPIN.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 0) {

                } else if (count == 1) {

                } else if (count == 2) {

                } else if (count == 3) {

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (etCountPIN.length() == 4) {
                    Intent intent = new Intent(SetPINActivity.this, MainDashboardActivity.class);
                    startActivity(intent);
                    finish();
                    etCountPIN.setText("");
                }

            }
        });
    }

    @OnClick({R.id.b1, R.id.b2, R.id.b3, R.id.b4, R.id.b5, R.id.b6, R.id.b7, R.id.b8, R.id.b9, R.id.b0, R.id.bDelete})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.b1:
                if (etCountPIN.length() < 4) {
                    etCountPIN.setText(etCountPIN.getText() + "1");
                    if (pin1.getText().equals("") && pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin1.setText("1");
                    } else if (!pin1.getText().equals("") && pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin2.setText("1");
                    } else if (!pin1.getText().equals("") && !pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin3.setText("1");
                    } else if (!pin1.getText().equals("") && !pin2.getText().equals("") && !pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin4.setText("1");
                    }

                }
                break;
            case R.id.b2:
                if (etCountPIN.length() < 4) {
                    etCountPIN.setText(etCountPIN.getText() + "2");
                    if (pin1.getText().equals("") && pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin1.setText("2");
                    } else if (!pin1.getText().equals("") && pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin2.setText("2");
                    } else if (!pin1.getText().equals("") && !pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin3.setText("2");
                    } else if (!pin1.getText().equals("") && !pin2.getText().equals("") && !pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin4.setText("2");
                    }

                }
                break;
            case R.id.b3:
                if (etCountPIN.length() < 4) {
                    etCountPIN.setText(etCountPIN.getText() + "3");
                    if (pin1.getText().equals("") && pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin1.setText("3");
                    } else if (!pin1.getText().equals("") && pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin2.setText("3");
                    } else if (!pin1.getText().equals("") && !pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin3.setText("3");
                    } else if (!pin1.getText().equals("") && !pin2.getText().equals("") && !pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin4.setText("3");
                    }
                }
                break;
            case R.id.b4:
                if (etCountPIN.length() <= 4) {
                    etCountPIN.setText(etCountPIN.getText() + "4");
                    if (pin1.getText().equals("") && pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin1.setText("4");
                    } else if (!pin1.getText().equals("") && pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin2.setText("4");
                    } else if (!pin1.getText().equals("") && !pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin3.setText("4");
                    } else if (!pin1.getText().equals("") && !pin2.getText().equals("") && !pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin4.setText("4");
                    }
                }
                break;
            case R.id.b5:
                if (etCountPIN.length() < 4) {
                    etCountPIN.setText(etCountPIN.getText() + "5");
                    if (pin1.getText().equals("") && pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin1.setText("5");
                    } else if (!pin1.getText().equals("") && pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin2.setText("5");
                    } else if (!pin1.getText().equals("") && !pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin3.setText("5");
                    } else if (!pin1.getText().equals("") && !pin2.getText().equals("") && !pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin4.setText("5");
                    }
                }
                break;
            case R.id.b6:
                if (etCountPIN.length() < 4) {
                    etCountPIN.setText(etCountPIN.getText() + "6");
                    if (pin1.getText().equals("") && pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin1.setText("6");
                    } else if (!pin1.getText().equals("") && pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin2.setText("6");
                    } else if (!pin1.getText().equals("") && !pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin3.setText("6");
                    } else if (!pin1.getText().equals("") && !pin2.getText().equals("") && !pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin4.setText("6");
                    }
                }
                break;
            case R.id.b7:
                if (etCountPIN.length() < 4) {
                    etCountPIN.setText(etCountPIN.getText() + "7");
                    if (pin1.getText().equals("") && pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin1.setText("7");
                    } else if (!pin1.getText().equals("") && pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin2.setText("7");
                    } else if (!pin1.getText().equals("") && !pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin3.setText("7");
                    } else if (!pin1.getText().equals("") && !pin2.getText().equals("") && !pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin4.setText("7");
                    }
                }
                break;
            case R.id.b8:
                if (etCountPIN.length() < 4) {
                    etCountPIN.setText(etCountPIN.getText() + "8");
                    if (pin1.getText().equals("") && pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin1.setText("8");
                    } else if (!pin1.getText().equals("") && pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin2.setText("8");
                    } else if (!pin1.getText().equals("") && !pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin3.setText("8");
                    } else if (!pin1.getText().equals("") && !pin2.getText().equals("") && !pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin4.setText("8");
                    }
                }
                break;
            case R.id.b9:
                if (etCountPIN.length() < 4) {
                    etCountPIN.setText(etCountPIN.getText() + "9");
                    if (pin1.getText().equals("") && pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin1.setText("9");
                    } else if (!pin1.getText().equals("") && pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin2.setText("9");
                    } else if (!pin1.getText().equals("") && !pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin3.setText("9");
                    } else if (!pin1.getText().equals("") && !pin2.getText().equals("") && !pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin4.setText("9");
                    }
                }
                break;
            case R.id.b0:
                if (etCountPIN.length() < 4) {
                    etCountPIN.setText(etCountPIN.getText() + "0");
                    if (pin1.getText().equals("") && pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin1.setText("0");
                    } else if (!pin1.getText().equals("") && pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin2.setText("0");
                    } else if (!pin1.getText().equals("") && !pin2.getText().equals("") && pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin3.setText("0");
                    } else if (!pin1.getText().equals("") && !pin2.getText().equals("") && !pin3.getText().equals("") && pin4.getText().equals("")) {
                        pin4.setText("0");
                    }
                }
                break;
            case R.id.bDelete:

                if (etCountPIN.getText().length() > 0) {
                    CharSequence currentText = etCountPIN.getText();
                    etCountPIN.setText(currentText.subSequence(0, currentText.length() - 1));
                } else {
                    etCountPIN.setText("");
                    break;
                }
        }
    }
}