package com.kurawal.chuyentien.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.kurawal.chuyentien.R;
import com.kurawal.chuyentien.core.Config;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dpapayas on 7/22/17.
 */

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.etMobileNumber)
    EditText etMobileNumber;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.btnActionLogin)
    Button btnActionLogin;
    @BindView(R.id.layFooterResgister)
    LinearLayout layFooterResgister;
    @BindView(R.id.btnActionRegister)
    Button btnActionRegister;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_login);
        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }


    }

    @OnClick({R.id.btnActionLogin, R.id.btnActionRegister})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.btnActionLogin:

                if (!etMobileNumber.getText().toString().equals("") && !etPassword.getText().toString().equals("")) {

                    Intent intent = new Intent(LoginActivity.this, MainDashboardActivity.class);
                    startActivity(intent);
                } else {
                    Config.showError(this, "Login Error");
                }
                break;
            case R.id.btnActionRegister:

                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);


                break;
        }


    }
}
