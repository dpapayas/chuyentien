package com.kurawal.chuyentien.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.kurawal.chuyentien.R;
import com.kurawal.chuyentien.util.CenteredToolbar;
import com.kurawal.chuyentien.util.customfont.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dpapayas on 7/25/17.
 */

public class GetPadiActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    CenteredToolbar toolbar;
    @BindView(R.id.btnCardPlus)
    RelativeLayout btnCardPlus;
    @BindView(R.id.tvWallet)
    TextView tvWallet;
    @BindView(R.id.btnActionGenerate)
    RelativeLayout btnActionGenerate;
    @BindView(R.id.ivQR)
    ImageView ivQR;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_getpaid);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnActionGenerate)
    public void onViewClicked() {
    }
}
