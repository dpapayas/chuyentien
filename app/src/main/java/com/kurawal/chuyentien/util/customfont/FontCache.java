package com.kurawal.chuyentien.util.customfont;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;

/**
 * Created by dpapayas on 7/13/17.
 */

public class FontCache {
    private static HashMap<String, Typeface> fontCache = new HashMap();

    public FontCache() {
    }

    public static Typeface getTypeface(Context context, String customFont) {
        Typeface typeface = (Typeface) fontCache.get(customFont);
        if (typeface == null) {
            try {
                typeface = Typeface.createFromAsset(context.getAssets(), customFont);
            } catch (Exception var4) {
                return null;
            }

            fontCache.put(customFont, typeface);
        }

        return typeface;
    }
}
