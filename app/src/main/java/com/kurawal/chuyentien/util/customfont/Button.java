package com.kurawal.chuyentien.util.customfont;

/**
 * Created by dpapayas on 7/13/17.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import com.thebrownarrow.customfont.FontCache;
import com.thebrownarrow.customfont.R.styleable;

public class Button extends AppCompatButton {
    public Button(Context context) {
        super(context);
    }

    public Button(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.applyCustomTypeface(context, attributeSet);
    }

    public Button(Context context, AttributeSet attributeSet, int defStyle) {
        super(context, attributeSet, defStyle);
        this.applyCustomTypeface(context, attributeSet);
    }

    private void applyCustomTypeface(Context context, AttributeSet attributeSet) {
        TypedArray typedArray = context.obtainStyledAttributes(attributeSet, styleable.CustomTypeface);
        String customFont = typedArray.getString(styleable.CustomTypeface_custom_typeface);
        this.applyCustomTypeface(context, customFont);
        typedArray.recycle();
    }

    public boolean applyCustomTypeface(Context context, String asset) {
        this.setTypeface(FontCache.getTypeface(context, asset));
        return true;
    }
}
